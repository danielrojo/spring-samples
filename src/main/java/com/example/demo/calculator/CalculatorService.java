package com.example.demo.calculator;

import org.springframework.stereotype.Service;

enum State {
    INIT, FIRST_NUMBER, SECOND_NUMBER, RESULT
}

@Service
public class CalculatorService {

    private State currentState = State.INIT;
    private int firstNumber = 0;
    private int secondNumber = 0;
    private int result = 0;
    private char operator = '?';

    CalculatorService() {
    }

    private void processNumber(int number) {
        switch (this.currentState) {
            case INIT:
                this.firstNumber = number;
                this.currentState = State.FIRST_NUMBER;
                break;
            case FIRST_NUMBER:
                this.firstNumber = this.firstNumber * 10 + number;
                break;
            case SECOND_NUMBER:
                this.secondNumber = this.secondNumber * 10 + number;
                break;
            case RESULT:
                break;
            default:
                break;
        }

    }

    public void processExpression(String expression) {

        for (int i = 0; i < expression.length(); i++) {
            char c = expression.charAt(i);
            // check if c is a digit
            if (Character.isDigit(c)) {
                // char to int
                int digit = Character.getNumericValue(c);
                this.processNumber(digit);
            } else {
                this.processSymbol(c);
            }

        }
    }

    private void processSymbol(char c) {
        switch (this.currentState) {
            case INIT:
                break;
            case FIRST_NUMBER:
                if (this.isOperator(c)) {
                    this.operator = c;
                    this.currentState = State.SECOND_NUMBER;
                }
                break;
            case SECOND_NUMBER:
                if (c == '=') {
                    this.result = this.solve();
                    this.currentState = State.INIT;
                }
                break;
            case RESULT:
                break;
            default:
                break;
        }

    }

    private boolean isOperator(char c) {
        return c == '+' || c == '-' || c == '*' || c == '/';
    }

    private int solve() {
        switch (this.operator) {
            case '+':
                return this.firstNumber + this.secondNumber;
            case '-':
                return this.firstNumber - this.secondNumber;
            case '*':
                return this.firstNumber * this.secondNumber;
            case '/':
                return this.firstNumber / this.secondNumber;
            default:
                return 0;
        }
    }

    public String calculatorToString() {
        return this.firstNumber + " " + this.operator + " " + this.secondNumber + " = " + this.result;
    }

    public void clearCalculator() {
        this.currentState = State.INIT;
        this.firstNumber = 0;
        this.secondNumber = 0;
        this.result = 0;
        this.operator = '?';
    }

    public double resolveOperation(int num1, double num2, String op) {
        double result = 0;
        switch (op) {
            case "+":
                result = num1 + num2;
                break;
            case "-":
                result = num1 - num2;
                break;
            case "*":
                result = num1 * num2;
                break;
            case "/":
                result = num1 / num2;
                break;
            default:
                return 0;
        }
        return Math.round(result * 100.0) / 100.0;

    }

}
