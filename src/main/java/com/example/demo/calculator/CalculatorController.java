package com.example.demo.calculator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class CalculatorController {

    @Autowired
    private CalculatorService calculatorService;

    @GetMapping("/calculator")
    public String calculator(
            @RequestParam(name = "num1", required = false, defaultValue = "0") String num1,
            @RequestParam(name = "num2", required = false, defaultValue = "0") String num2,
            @RequestParam(name = "op", required = false, defaultValue = "+") String op,
            Model view) {
        int myNum1 = Integer.parseInt(num1);
        double myNum2 = Double.parseDouble(num2);
        double result = this.calculatorService.resolveOperation(myNum1, myNum2, op);
        // limit result to 2 decimal places
        view.addAttribute("result", result);
        view.addAttribute("title", "Hola mundo!");
        view.addAttribute("num1", num1);
        view.addAttribute("num2", num2);
        view.addAttribute("op", op);
        return "calculator";
    }

    @GetMapping("/evaluate")
    public String evaluate(
            @RequestParam(name = "exp", required = false, defaultValue = "23+456=") String expression,
            Model view) {
        this.calculatorService.processExpression(expression);
        String myExpression = this.calculatorService.calculatorToString();
        this.calculatorService.clearCalculator();
        view.addAttribute("expression", myExpression);
        return "evaluate";
    }

}
