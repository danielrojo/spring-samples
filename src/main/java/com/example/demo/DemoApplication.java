package com.example.demo;

import org.modelmapper.ModelMapper;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

import com.example.demo.beers.beer.BeerDto;
import com.example.demo.beers.beer.BeerEntity;
import com.example.demo.beers.beer.BeersService;
import com.example.demo.library.book.BookEntity;
import com.example.demo.library.book.BookRepository;
import com.example.demo.library.user.UserDto;
import com.example.demo.library.user.UserEntity;
import com.example.demo.library.user.UserService;

@SpringBootApplication(exclude = { SecurityAutoConfiguration.class })
public class DemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}

	@Bean
    public ModelMapper modelMapper() {
        return new ModelMapper();
    }

	
	public CommandLineRunner demo(BeersService beersService, UserService userService, BookRepository bookRepository) {
		return (args) -> {
			// save a couple of beers
			for(BeerDto beer : beersService.getBeersFromApi()) {
				beersService.addBeer(beer);
			}

			UserDto user = UserDto.builder()
				.firstName("uno")
				.lastName("cualquiera")
				.email("uno@cualquiera.com")
				.build();
			userService.addUser(user);

			BookEntity book = BookEntity.builder()
				.title("El Quijote")
				.author("Cervantes")
				.isbn("123456789")
				.build();
			
			bookRepository.save(book);

			book = BookEntity.builder()
				.title("El Quijote 2")
				.author("Cervantes")
				.isbn("123456789")
				.build();

			bookRepository.save(book);
		};



	}


}
