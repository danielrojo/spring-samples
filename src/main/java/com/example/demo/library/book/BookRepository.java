package com.example.demo.library.book;

import org.springframework.data.repository.CrudRepository;

public interface BookRepository extends CrudRepository<BookEntity, Long> {
    BookEntity findById(long id);
}
