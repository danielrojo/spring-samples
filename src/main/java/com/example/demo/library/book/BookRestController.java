package com.example.demo.library.book;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import jakarta.transaction.Transactional;

@RestController
@RequestMapping("/api/v1")
@Transactional
public class BookRestController {

    @Autowired
    private BookService bookService;

    @GetMapping("/books")
    public Iterable<BookDto> getBooks() {
        return bookService.getBooks();
    }

    @PostMapping("/books")
    public BookDto addBook(@RequestBody BookDto book) {
        return bookService.addBook(book);
    }

    @PutMapping("/books/{id}")
    public BookDto updateBook(@RequestBody BookDto book, @PathVariable long id) {
        return bookService.updateBook(book, id);
    }

    @GetMapping("/books/{id}")
    public BookDto getBookById(@PathVariable long id) {
        return bookService.deleteBook(id);
    }
    
}
