package com.example.demo.library.book;

import com.example.demo.library.lend.LendEntity;
import com.fasterxml.jackson.annotation.JsonIgnore;

import io.micrometer.common.lang.NonNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BookDto {

    @NonNull
    private String author;
    @NonNull
    private String title;
    @NonNull
    private String isbn;

    @JsonIgnore
    Iterable<LendEntity> lend;
}
