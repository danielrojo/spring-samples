package com.example.demo.library.book; 

import java.util.ArrayList;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BookService {

    @Autowired
    private BookRepository repository;

    @Autowired
    private ModelMapper modelMapper;

    public Iterable<BookDto> getBooks() {
        Iterable<BookEntity> books = repository.findAll();
        Iterable<BookDto> response = new ArrayList<>();
        for (BookEntity book : books) {
            BookDto bookDto = modelMapper.map(book, BookDto.class);
            ((ArrayList<BookDto>) response).add(bookDto);
        }
        return response;
    }

    public BookDto getBookById(long id) {
        BookEntity book = repository.findById(id);
        if (book == null) {
            return null;
        } else {
            return modelMapper.map(book, BookDto.class);
        }
    }

    public BookEntity getBookEntityById(long id) {
        BookEntity book = repository.findById(id);
        return book;
    }


    public BookDto addBook(BookDto book) {
        BookEntity bookEntity = repository.save(modelMapper.map(book, BookEntity.class));
        return modelMapper.map(bookEntity, BookDto.class);

    }

    public BookDto updateBook(BookDto book, long id) {
        BookEntity bookEntity = repository.findById(id);
        if(bookEntity == null) {
            return null;
        } else {
            bookEntity.setAuthor(book.getAuthor());
            bookEntity.setTitle(book.getTitle());
            bookEntity.setIsbn(book.getIsbn());
            BookEntity savedBook = repository.save(bookEntity);
            return modelMapper.map(savedBook, BookDto.class);
        }
    }

    public BookDto deleteBook(long id) {
        BookEntity book = repository.findById(id);
        if(book == null) {
            return null;
        } else {
            repository.delete(book);
            return modelMapper.map(book, BookDto.class);
        }
    }

    
}
