package com.example.demo.library.book;

import java.util.List;

import com.example.demo.library.lend.LendEntity;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

@Entity
@Table(name="book")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BookEntity {
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @NonNull
    private String author;
    @NonNull
    private String title;
    @NonNull
    private String isbn;

    @OneToMany(mappedBy = "book")
    List<LendEntity> lend;


}
