package com.example.demo.library.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import jakarta.servlet.http.HttpServletResponse;
import jakarta.transaction.Transactional;


@RestController
@RequestMapping("/api/v1")
@Transactional
public class UserRestController {

    @Autowired
    private UserService userService;

    @GetMapping("/users")
    public Iterable<UserDto> getUser() {
        return userService.getUsers();
    }

    @PostMapping("/users")
    public ResponseEntity<UserDto> addUser(@RequestBody UserDto user) {
        UserDto myUser = userService.findByEmail(user.getEmail());
        if (myUser == null) {
            return ResponseEntity.ok(userService.addUser(user));
        } else {
            return ResponseEntity.status(HttpServletResponse.SC_CONFLICT).body(user);
        }
    }

    @PutMapping("/users/{id}")
    public ResponseEntity<UserDto> updateUser(@RequestBody UserEntity user) {
        UserDto myUser = userService.findByEmail(user.getEmail());
        if (myUser != null) {
            return ResponseEntity.ok(userService.updateUser(myUser));
        } else {
            return ResponseEntity.status(HttpServletResponse.SC_NOT_FOUND).body(null);
        }
    }

    @DeleteMapping("/users/{id}")
    public ResponseEntity<UserDto> deleteById(@PathVariable long id) {
        UserDto myUser = userService.findById(id);
        if (myUser != null) {
            userService.deleteById(id);
            return ResponseEntity.ok(myUser);
        } else {
            return ResponseEntity.status(HttpServletResponse.SC_NOT_FOUND).body(null);
        } 
    }
    
}