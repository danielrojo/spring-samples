package com.example.demo.library.user;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jakarta.validation.Valid;
import lombok.extern.java.Log;
import lombok.extern.slf4j.Slf4j;

@Service
public class UserService {
    
    @Autowired
    UserRepository repository;

    public Iterable<UserDto> getUsers() {
        Iterable<UserEntity> user = repository.findAll();
        Iterable<UserDto> response = new ArrayList<>();
        for(UserEntity u : user) {
            UserDto userDto = UserDto.builder()
                .firstName(u.getFirstName())
                .lastName(u.getLastName())
                .email(u.getEmail())
                .build();
            ((ArrayList<UserDto>) response).add(userDto);
        }

        return response;

    }

    public UserDto getUserById(long id) {
        UserEntity user = repository.findById(id);
        return UserDto.builder()
            .firstName(user.getFirstName())
            .lastName(user.getLastName())
            .email(user.getEmail())
            .build();
    }

    public UserDto getUserByEmail(String email) {
        UserEntity user = repository.findByEmail(email);
        return UserDto.builder()
            .firstName(user.getFirstName())
            .lastName(user.getLastName())
            .email(user.getEmail())
            .build();
    }

    public UserDto addUser(UserDto user) {
        UserEntity userEntity = UserEntity.builder()
            .firstName(user.getFirstName())
            .lastName(user.getLastName())
            .email(user.getEmail())
            .build();
        repository.save(userEntity);
        return user;
    }

	public void addUserForm(@Valid UserForm user) {
        UserEntity userEntity = UserEntity.builder()
            .firstName(user.getFirstName())
            .lastName(user.getLastName())
            .email(user.getEmail())
            .build();
        repository.save(userEntity);
    }

    public UserDto updateUser(UserDto user) {
        UserEntity userEntity = repository.findByEmail(user.getEmail());
        if(userEntity != null) {
            userEntity.setFirstName(user.getFirstName());
            userEntity.setLastName(user.getLastName());
            repository.save(userEntity);
            return user;
        }else {
            return null;
        }
    }

    public void deleteUser(long id) {
        repository.deleteById(id);
	}

    public UserDto findByEmail(String email) {
        UserEntity user = repository.findByEmail(email);
        if(user != null) {
            return UserDto.builder()
                .firstName(user.getFirstName())
                .lastName(user.getLastName())
                .email(user.getEmail())
                .build();
        }else {
            return null;
        }
    }

    public long getIdByEmail(String email) {
        UserEntity user = repository.findByEmail(email);
        if(user != null) {
            return user.getId();
        }else {
            return -1;
        }
    }

    public void deleteById(long id) {
        repository.deleteById(id);
    }

    public UserDto findById(long id) {
        UserEntity user = repository.findById(id);
        if(user != null) {
            return UserDto.builder()
                .firstName(user.getFirstName())
                .lastName(user.getLastName())
                .email(user.getEmail())
                .build();
        }else {
            return null;
        }
    }

    public UserEntity getUserEntityById(Long id) {
        Optional<UserEntity> user = repository.findById(id);
        if(user.isPresent()) {
            return user.get();
        }else {
            return null;
        }
    }
}
