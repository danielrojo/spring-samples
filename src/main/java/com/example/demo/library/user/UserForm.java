package com.example.demo.library.user;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserForm {

    @Size(min = 2, max = 255)
    private String firstName;
    @Size(min = 2, max = 255)
    private String lastName;
    @Email
    private String email;

    public UserForm() {
    }

    public UserForm(String firstName, String lastName, String email) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
    }

    @Override
    public String toString() {
        return "UserForm [email=" + email + ", firstName=" + firstName + ", lastName=" + lastName + "]";
    }
    
}
