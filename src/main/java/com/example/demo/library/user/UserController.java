package com.example.demo.library.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import com.example.demo.beers.beer.BeerEntity;

import jakarta.validation.Valid;

@Controller
public class UserController {
    @Autowired
    private UserService service;

    @GetMapping("/users")
    public String getUsers(Model view) {
        view.addAttribute("users", service.getUsers());
        return "users";
    }

    @GetMapping("/adduser")
    public String addUser(Model view) {
        view.addAttribute("user", new UserForm("uno", "cualquiera", "uno@cualquiera.com"));
        return "adduser";
    }

    @PostMapping("/adduser")
    public String addUser(@ModelAttribute @Valid UserForm user, Model view) {
        service.addUserForm(user);
        return "redirect:/users";
    }

}
