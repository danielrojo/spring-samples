package com.example.demo.library.lend;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.library.book.BookDto;
import com.example.demo.library.book.BookService;
import com.example.demo.library.user.UserDto;
import com.example.demo.library.user.UserService;

@Service
public class LendService {

    @Autowired
    private LendRepository lendRepository;

    @Autowired
    private BookService bookService;

    @Autowired
    private UserService userService;

    public LendDto getLend(long id) {
        LendEntity lend = lendRepository.findById(id);
        if(lend == null) {
            return null;
        } else {
            return LendDto.builder()
                    .book(bookService.getBookById(lend.getBook().getId()))
                    .user(userService.getUserById(lend.getUser().getId()))
                    .build();
        }
    }

    public Iterable<LendDto> getLends() {
        Iterable<LendEntity> lends = lendRepository.findAll();
        Iterable<LendDto> response = new ArrayList<>();
        for(LendEntity lend : lends) {
            LendDto lendDto = LendDto.builder()
                    .book(bookService.getBookById(lend.getBook().getId()))
                    .user(userService.getUserById(lend.getUser().getId()))
                    .build();
            ((ArrayList<LendDto>) response).add(lendDto);
        }
        return response;
    }

    public BookDto getBookById(long id) {
        LendEntity lend = lendRepository.findById(id);
        if(lend == null) {
            return null;
        } else {
            return bookService.getBookById(lend.getBook().getId());
        }
    }

    public UserDto getUserById(long id) {
        LendEntity lend = lendRepository.findById(id);
        if(lend == null) {
            return null;
        } else {
            return userService.getUserById(lend.getUser().getId());
        }
    }

    public LendDto addLend(LendRequest lend){
        // date str yyyy-MM-dd to Date
        LendEntity lendEntity = LendEntity.builder()
                .book(bookService.getBookEntityById(lend.getBookId()))
                .user(userService.getUserEntityById(lend.getUserId()))
                .lendDate(lend.getLendDate())
                .dueDate(lend.getDueDate())
                .build();
        lendRepository.save(lendEntity);
        // Date to str yyyy-MM-dd
        return LendDto.builder()
                .book(bookService.getBookById(lendEntity.getBook().getId()))
                .user(userService.getUserById(lendEntity.getUser().getId()))
                .lendDate(lend.getLendDate())
                .dueDate(lend.getDueDate())
                .build();
    }

    public LendDto updateLend(long id, LendRequest lend) {
        LendEntity lendEntity = lendRepository.findById(id);
        if(lendEntity == null) {
            return null;
        } else {
            lendEntity.setBook(bookService.getBookEntityById(lend.getBookId()));
            lendEntity.setUser(userService.getUserEntityById(lend.getUserId()));
            lendRepository.save(lendEntity);
            return LendDto.builder()
                    .book(bookService.getBookById(lendEntity.getBook().getId()))
                    .user(userService.getUserById(lendEntity.getUser().getId()))
                    .build();
        }
    }
    
}
