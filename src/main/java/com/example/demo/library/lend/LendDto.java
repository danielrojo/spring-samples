package com.example.demo.library.lend;

import com.example.demo.library.book.BookDto;
import com.example.demo.library.user.UserDto;
import com.example.demo.library.user.UserEntity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class LendDto {

    BookDto book;

    UserDto user;

    @JsonProperty("lend_date")
    String lendDate;

    @JsonProperty("due_date")
    String dueDate;

    
    
}
