package com.example.demo.library.lend;

import org.springframework.data.repository.CrudRepository;

public interface LendRepository extends CrudRepository<LendEntity, Long> {

    LendEntity findById(long id);
}
