package com.example.demo.beers.customer;


import java.util.List;

public interface CustomerRepository extends org.springframework.data.repository.CrudRepository<CustomerEntity, Long>{
    CustomerEntity findById(long id);
    CustomerEntity findByEmail(String email);
    List<CustomerEntity> findByFirstName(String firstName);
    List<CustomerEntity> findByLastName(String lastName);
}