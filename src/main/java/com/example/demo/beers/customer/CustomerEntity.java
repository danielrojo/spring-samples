package com.example.demo.beers.customer;

import java.util.List;

import com.example.demo.beers.purchase.PurchaseEntity;
import com.example.demo.library.lend.LendEntity;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@AllArgsConstructor
@RequiredArgsConstructor
@Builder
@Table(name="customer")
public class CustomerEntity {

    @Id
    @GeneratedValue(strategy = jakarta.persistence.GenerationType.AUTO)
    private long id;
    
    private String firstName;
    private String lastName;
    private String email;

    @OneToMany(mappedBy = "customer", fetch = jakarta.persistence.FetchType.LAZY)
    List<PurchaseEntity> purchases;

}
