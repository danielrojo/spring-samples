package com.example.demo.beers.customer;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CustomerService {
    
    @Autowired
    CustomerRepository repository;

    public Iterable<CustomerDto> getCustomers() {
        Iterable<CustomerEntity> customers = repository.findAll();
        Iterable<CustomerDto> response = new ArrayList<>();
        for(CustomerEntity u : customers) {
            CustomerDto customer = CustomerDto.builder()
                .firstName(u.getFirstName())
                .lastName(u.getLastName())
                .email(u.getEmail())
                .build();
            ((ArrayList<CustomerDto>) response).add(customer);
        }
        return response;

    }

    public CustomerDto getCustomerById(long id) {
        CustomerEntity customer = repository.findById(id);
        return CustomerDto.builder()
            .firstName(customer.getFirstName())
            .lastName(customer.getLastName())
            .email(customer.getEmail())
            .build();
    }

    public CustomerDto getCustomerByEmail(String email) {
        CustomerEntity customer = repository.findByEmail(email);
        return CustomerDto.builder()
            .firstName(customer.getFirstName())
            .lastName(customer.getLastName())
            .email(customer.getEmail())
            .build();
    }

    public CustomerDto addCustomer(CustomerDto customer) {
        CustomerEntity myCustomer = CustomerEntity.builder()
            .firstName(customer.getFirstName())
            .lastName(customer.getLastName())
            .email(customer.getEmail())
            .build();
        repository.save(myCustomer);
        return customer;
    }

    public CustomerDto updateCustomer(CustomerDto customer) {
        CustomerEntity myCustomer = repository.findByEmail(customer.getEmail());
        if(customer != null) {
            myCustomer.setFirstName(customer.getFirstName());
            myCustomer.setLastName(customer.getLastName());
            repository.save(myCustomer);
            return customer;
        }else {
            return null;
        }
    }

    public void deleteCustomer(long id) {
        repository.deleteById(id);
	}

    public CustomerDto findCustomerByEmail(String email) {
        CustomerEntity customer = repository.findByEmail(email);
        if(customer != null) {
            return CustomerDto.builder()
                .firstName(customer.getFirstName())
                .lastName(customer.getLastName())
                .email(customer.getEmail())
                .build();
        }else {
            return null;
        }
    }

    public long getCustomerIdByEmail(String email) {
        CustomerEntity customer = repository.findByEmail(email);
        if(customer != null) {
            return customer.getId();
        }else {
            return -1;
        }
    }

    public void deleteCustomerById(long id) {
        repository.deleteById(id);
    }

    public CustomerDto findCustomerById(long id) {
        CustomerEntity customer = repository.findById(id);
        if(customer != null) {
            return CustomerDto.builder()
                .firstName(customer.getFirstName())
                .lastName(customer.getLastName())
                .email(customer.getEmail())
                .build();
        }else {
            return null;
        }
    }

    public CustomerEntity getCustomerEntityById(Long id) {
        Optional<CustomerEntity> customer = repository.findById(id);
        if(customer.isPresent()) {
            return customer.get();
        }else {
            return null;
        }
    }
}
