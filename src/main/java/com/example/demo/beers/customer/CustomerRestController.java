package com.example.demo.beers.customer;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import jakarta.servlet.http.HttpServletResponse;
import jakarta.transaction.Transactional;


@RestController
@RequestMapping("/api/v1")
@Transactional
public class CustomerRestController {

    @Autowired
    private CustomerService userService;

    @GetMapping("/customers")
    public Iterable<CustomerDto> getUser() {
        return userService.getCustomers();
    }

    @PostMapping("/customers")
    public ResponseEntity<CustomerDto> addUser(@RequestBody CustomerDto user) {
        CustomerDto myUser = userService.findCustomerByEmail(user.getEmail());
        if (myUser == null) {
            return ResponseEntity.ok(userService.addCustomer(user));
        } else {
            return ResponseEntity.status(HttpServletResponse.SC_CONFLICT).body(null);
        }
    }

    @PutMapping("/customers/{id}")
    public ResponseEntity<CustomerDto> updateUser(@RequestBody CustomerEntity user) {
        CustomerDto myUser = userService.findCustomerByEmail(user.getEmail());
        if (myUser != null) {
            return ResponseEntity.ok(userService.updateCustomer(myUser));
        } else {
            return ResponseEntity.status(HttpServletResponse.SC_NOT_FOUND).body(null);
        }
    }

    @DeleteMapping("/customers/{id}")
    public ResponseEntity<CustomerDto> deleteById(@PathVariable long id) {
        CustomerDto myUser = userService.findCustomerById(id);
        if (myUser != null) {
            userService.deleteCustomerById(id);
            return ResponseEntity.ok(myUser);
        } else {
            return ResponseEntity.status(HttpServletResponse.SC_NOT_FOUND).body(null);
        } 
    }
    
}