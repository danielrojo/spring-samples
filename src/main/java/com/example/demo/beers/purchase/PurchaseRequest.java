package com.example.demo.beers.purchase;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@AllArgsConstructor
@RequiredArgsConstructor
@Builder
public class PurchaseRequest {

    private List<Long> beerIds;

    private long customerId;

    private String date;
    private String price;
    private String reference;
    
}