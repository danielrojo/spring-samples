package com.example.demo.beers.purchase;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1")
public class PurchaseRestController {

    @Autowired
    private PurchaseService purchaseService;

    @GetMapping("/purchases")
    public Iterable<PurchaseDto> getPurchases() {
        return purchaseService.getPurchases();
    }

    @GetMapping("/purchases/{id}")
    public PurchaseDto getPurchase(@PathVariable long id) {
        return purchaseService.getPurchase(id);
    }

    @PostMapping("/purchases")
    public PurchaseDto addPurchase(PurchaseRequest purchase) {
        return purchaseService.addPurchase(purchase);
    }

    @DeleteMapping("/purchases/{id}")
    public void deletePurchase(@PathVariable long id) {
        purchaseService.deletePurchase(id);
    }


    
}
