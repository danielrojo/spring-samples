package com.example.demo.beers.purchase;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.beers.beer.BeerEntity;
import com.example.demo.beers.beer.BeersService;
import com.example.demo.beers.customer.CustomerService;

@Service
public class PurchaseService {

    @Autowired
    private PurchaseRepository purchaseRepository;

    @Autowired
    private CustomerService customerService;

    @Autowired
    private BeersService beerService;

    @Autowired
    private ModelMapper modelMapper;

    public PurchaseDto getPurchase(long id) {
        PurchaseEntity purchase = purchaseRepository.findById(id);
        if (purchase == null) {
            return null;
        } else {
            return modelMapper.map(purchase, PurchaseDto.class);
        }
    }

    public Iterable<PurchaseDto> getPurchases() {
        Iterable<PurchaseEntity> purchases = purchaseRepository.findAll();
        Iterable<PurchaseDto> response = new Iterable<PurchaseDto>() {
            @Override
            public java.util.Iterator<PurchaseDto> iterator() {
                return null;
            }
        };
        for (PurchaseEntity purchase : purchases) {
            PurchaseDto purchaseDto = modelMapper.map(purchase, PurchaseDto.class);
            ((java.util.ArrayList<PurchaseDto>) response).add(purchaseDto);
        }
        return response;
    }

    public PurchaseDto addPurchase(PurchaseRequest purchase) {
        PurchaseEntity purchaseEntity = new PurchaseEntity();
        purchaseEntity.setReference(purchase.getReference());
        purchaseEntity.setCustomer(customerService.getCustomerEntityById(purchase.getCustomerId()));
        // purchaseEntity.setBeer(beerService.getBeerById(purchase.getBeerId()));
        List<BeerEntity> beers = new ArrayList<>();
        for (long beerId : purchase.getBeerIds()) {
            beers.add(beerService.getBeerEntityById(beerId));
        }
        purchaseEntity.setBeers(beers);
        PurchaseEntity savedPurchase = purchaseRepository.save(purchaseEntity);
        return modelMapper.map(savedPurchase, PurchaseDto.class);

    }

    public void deletePurchase(long id) {
        PurchaseEntity purchaseEntity = purchaseRepository.findById(id);
        if (purchaseEntity != null) {
            purchaseRepository.delete(purchaseEntity);
        }
    }

}
