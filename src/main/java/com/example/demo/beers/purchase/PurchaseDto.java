package com.example.demo.beers.purchase;

import java.util.List;

import com.example.demo.beers.beer.BeerEntity;
import com.example.demo.beers.customer.CustomerEntity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@AllArgsConstructor
@RequiredArgsConstructor
@Builder
public class PurchaseDto {

    private List<BeerEntity> beer;

    private CustomerEntity user;

    private String date;
    private String price;
    private String reference;
    
}