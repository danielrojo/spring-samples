package com.example.demo.beers.purchase;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

public interface PurchaseRepository extends JpaRepository<PurchaseEntity, Long>{
    PurchaseEntity findById(long id);
    List<PurchaseEntity> findAll();
    // BeerPurchaseEntity FindByReference(String reference);

}