package com.example.demo.beers.beer;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import org.springframework.web.client.RestTemplate;

@Service
public class BeersService {

    @Autowired
    private BeersRepository beersRepository;

    @Autowired
    private RestTemplate resTemplate;

    public Iterable<BeerDto> getAllBeers(double abvGt, double abvLt) {
        Iterable<BeerEntity> allBeers = beersRepository.findByAbvRange(abvGt, abvLt);
        // filter the beers
        List<BeerDto> result = new ArrayList<BeerDto>();
        for (BeerEntity beer : allBeers) {
            result.add(BeerDto.builder()
                    .name(beer.getName())
                    .description(beer.getDescription())
                    .tagline(beer.getTagline())
                    .firstBrewed(beer.getFirstBrewed())
                    .abv(beer.getAbv()).build());
        }
        return result;
    }

    public BeerDto getBeerById(long id) {
        BeerEntity beer = beersRepository.findById(id);
        if (beer == null) {
            return null;
        } else {
            return BeerDto.builder().name(beer.getName()).description(beer.getDescription()).tagline(beer.getTagline())
                    .firstBrewed(beer.getFirstBrewed()).abv(beer.getAbv()).build();
        }
    }

    public BeerEntity getBeerEntityById(long id) {
        BeerEntity beer = beersRepository.findById(id);
        if (beer == null) {
            return null;
        } else {
            return beer;
        }
    }

    public BeerDto getBeerByBeerName(String beerName) {
        BeerEntity beer = beersRepository.findByName(beerName);
        if (beer == null) {
            return null;
        } else {
            return BeerDto.builder().name(beerName).description(beer.getDescription()).tagline(beer.getTagline())
                    .firstBrewed(beer.getFirstBrewed()).abv(beer.getAbv()).build();
        }
    }

    public Iterable<BeerDto> getBeersFromApi() {
        List<BeerEntity> result = new ArrayList<BeerEntity>();
        BeerDto[] beers = resTemplate.getForObject("https://api.punkapi.com/v2/beers", BeerDto[].class);
        List<BeerDto> myResult = new ArrayList<BeerDto>();

        for (BeerDto beer : beers) {
            BeerEntity beerEntity = BeerEntity.builder().name(beer.getName()).description(beer.getDescription())
                    .tagline(beer.getTagline()).firstBrewed(beer.getFirstBrewed()).abv(beer.getAbv()).build();
            result.add(beerEntity);
            myResult.add(beer);
        }

        return myResult;
    }

    public BeerDto updateBeer(BeerDto beer, long id) {
        // remove the old beer
        BeerEntity beerEntity = beersRepository.findById(id);
        if (beerEntity == null) {
            throw new RuntimeException("Beer not found");
        } else {
            beersRepository.delete(beerEntity);
            beerEntity = new BeerEntity(id, beer.getName(), beer.getTagline(), beer.getFirstBrewed(),
                    beer.getDescription(), beer.getImageUrl(), beer.getAbv(), beer.getIbu(), beer.getEbc(), null);
            beersRepository.save(beerEntity);
            return beer;
        }
    }

    public BeerDto addBeer(BeerDto beer) {
        BeerEntity beerEntity = BeerEntity.builder().name(beer.getName()).description(beer.getDescription())
                .tagline(beer.getTagline()).firstBrewed(beer.getFirstBrewed()).abv(beer.getAbv()).build();
        beersRepository.save(beerEntity);
        return beer;
    }

    public BeerDto addBeer(BeerEntity beer) {
        BeerEntity beerSaved = beersRepository.save(beer);
        return BeerDto.builder().name(beerSaved.getName()).description(beerSaved.getDescription())
                .tagline(beerSaved.getTagline()).firstBrewed(beerSaved.getFirstBrewed()).abv(beerSaved.getAbv())
                .build();
    }

    public BeerDto deleteBeer(long id) {
        BeerEntity beerEntity = beersRepository.findById(id);
        if (beerEntity == null) {
            throw new RuntimeException("Beer not found");
        } else {
            beersRepository.delete(beerEntity);
            return BeerDto.builder().name(beerEntity.getName()).description(beerEntity.getDescription())
                    .tagline(beerEntity.getTagline()).firstBrewed(beerEntity.getFirstBrewed()).abv(beerEntity.getAbv())
                    .build();
        }

    }

}
