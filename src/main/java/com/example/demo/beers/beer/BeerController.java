package com.example.demo.beers.beer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import jakarta.validation.Valid;

@Controller
public class BeerController {
    @Autowired
    private BeersService service;

    @GetMapping("/beers")
    public String greeting(Model view) {
        view.addAttribute("beers", service.getAllBeers(0,100));
        return "beers";
    }

    @GetMapping("/beers/{id}/info")
    public String getInfo(@PathVariable long id, Model view) {
        view.addAttribute("beer", service.getBeerById(id));
        return "beerinfo";
    }


    @GetMapping("/addbeer")
    public String addBeer(Model view) {
        view.addAttribute("beer", new BeerDto());
        return "addbeer";
    }


}
