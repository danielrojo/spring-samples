package com.example.demo.beers.beer;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;


@Repository
public interface BeersRepository extends JpaRepository<BeerEntity, Long> {
    BeerEntity findById(long id);
    BeerEntity findByName(String beerName);
    List<BeerEntity> findByAbvBetween(double less, double greater);

    @Query("SELECT b FROM BeerEntity b WHERE b.abv > :abvGt AND b.abv < :abvLt")
    List<BeerEntity> findByAbvRange(@Param("abvGt") Double abvg, @Param("abvLt") Double abvl);
}
